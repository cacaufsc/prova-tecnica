# Teste de Conhecimentos - Analista Desenvolvedor#

## Questão 1 ##
O arquivo da questão 1 é "prova-tecnica\exercicio01.php"

#### Rodar o arquivo ####
O arquivo pode ser rodado via:

1 Terminal
```
#!bash
php exercicio01.php

```
2 Browser

Ex: http://localhost/prova-tecnica/exercicio01.php

## Questão 2 ##
O arquivo da questão 2 é "prova-tecnica\exercicio02.php" e trata-se de uma refatoração de parte de um código, não pode ser rodado individualmente

## Questão 3 ##
O arquivo da questão 3 é "prova-tecnica\exercicio03.php" e trata-se de uma refatoração de parte de um código, não pode ser rodado individualmente

##  Questão 4 ##

### Instalação Sistema Gerenciador de Tarefas ###

#### 1 - Criar Banco de dados: "tasks"  ####

SQL - Banco de Dados

```
#!sql

CREATE TABLE `tarefas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `descricao` text,
  `prioridade` smallint(6) DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
);
```
#### 2 - Configurar dados de acesso ao banco de dados em "prova-tecnica\tasks\app\Config\database.php"  ####

#### 3- Rodar o projeto ####
* Após clonar o repositório, o projeto pode ser acesso via Browser, dependendo do local de instalação:
* Exemplo Windows + Xampp: 

```
#!txt
Link acesso: http://localhost/prova-tecnica/tasks/
Local projeto: C:\xampp\htdocs\prova-tecnica\tasks\
```

### Frameworks e bibliotecas ###

* Cakephp 2.6
* Bootstrap 3.3.4