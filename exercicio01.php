<?php
/** 
*Escreva um programa que imprima números de 1 a 100. Mas, para múltiplos de 3 imprima
*“Fizz” em vez do número e para múltiplos de 5 imprima “Buzz”. Para números múltiplos
*de ambos (3 e 5), imprima “FizzBuzz”.
*/

function f() 
{
    for ($i = 1; $i <= 100; $i++) {
        $string = "";
        
        if ($i % 3 == 0) {
            $string .= "Fizz";
        }
        if ($i % 5 == 0) {
            $string .= "Buzz";
        }

        echo empty($string) ? $i : $string;
        echo '<br />';
    }
}

//Imprimir 1..100
f();
