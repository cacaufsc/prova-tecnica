<?php
App::uses('AppController', 'Controller');

/**
 * Tarefas Controller
 *
 * @property Tarefa $Tarefa
 * @property RequestHandlerComponent $RequestHandler
 */
class RestTarefasController extends AppController {

    public $uses = array('Tarefa');
    public $components = array('RequestHandler');

    /**
     * Index method
     */
    public function index() 
    {
        $tarefas = $this->Tarefa->find('all', array('order' => array('Tarefa.prioridade')));
        $this->set(array(
            'tarefas' => $tarefas,
            '_serialize' => array('tarefas')
        ));
    }

    /**
     * view method
     */
    public function view($id = null) 
    {
        $options = array('conditions' => array('Tarefa.' . $this->Tarefa->primaryKey => $id));
        $tarefa = $this->Tarefa->find('first', $options);
        $this->set(array(
            'tarefa' => $tarefa,
            '_serialize' => array('tarefa')
        ));
    }

    /**
     * add method
     *
     * @return boolean $result
     */
    public function add() 
    {
        $this->Tarefa->create();
        $result = $this->Tarefa->save($this->request->data) ? true : false;

        $this->set(array(
            'result' => $result,
            '_serialize' => array('result')
        ));
    }

    /**
     * edit method
     *
     * @param string $id
     * @return boolean $result
     */
    public function edit($id = null) 
    {
        $this->Tarefa->id = $id;
        $result = $this->Tarefa->save($this->request->data) ? true : false;
        $this->set(array(
            'result' => $result,
            '_serialize' => array('result')
        ));
    }

    /**
     * delete method
     *
     * 
     * @param string $id
     * @return void
     */
    public function delete($id = null) 
    {
        $result = $this->Tarefa->delete($id) ? true : false;
        $this->set(array(
            'result' => $result,
            '_serialize' => array('result')
        ));
    }
}
