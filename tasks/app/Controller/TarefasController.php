<?php
App::uses('HttpSocket', 'Network/Http');
App::uses('AppController', 'Controller');
/**
 * Tarefas Controller
 *
 * @property Tarefa $Tarefa
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class TarefasController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('RequestHandler');

    /**
     * index method
     *
     */
    public function index() 
    {
        $httpSocket = new HttpSocket();
        $link = $this->get_url('rest_tarefas.json');
        $response = $httpSocket->get($link);
        
        $this->set('tarefas', json_decode($response->body));
    }

    /**
     * view method
     *
     */
    public function view($id = null) 
    {
        $httpSocket = new HttpSocket();
        $link = $this->get_url("rest_tarefas/$id.json");
        $response = $httpSocket->get($link);

        $this->set('tarefa', json_decode($response->body));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() 
    {
        $httpSocket = new HttpSocket();

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $link = $this->get_url('rest_tarefas.json');
            $response = $httpSocket->post($link, $data );
            $result = json_decode($response->body);

            if ($result->result) {
                $this->Session->setFlash(__('A Tarefa foi salva'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('A tarefa não pôde ser salva. Por favor, tente novamente.'));
            }
        }
    }

    /**
     * edit method
     *
     * @param string $id
     * @return void
     */
    public function edit($id = null) 
    {
        $httpSocket = new HttpSocket();

        if ($this->request->is(array('post', 'put'))) {
            $data = $this->request->data;   
            $link = $this->get_url("rest_tarefas/$id.json");
            $response = $httpSocket->put($link, $data );
            $result = json_decode($response->body);

            if ($result->result) {
                $this->Session->setFlash(__('A tarefa foi salva.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('A tarefa não pôde ser salva. Por favor, tente novamente.'));  
            }

        } else {
            $response = $httpSocket->get($this->get_url("rest_tarefas/$id.json"));
            $result =  json_decode($response->body, true);
            $this->request->data = $result['tarefa'];
        }
    }

    /**
     * delete method
     *
     * @param string $id
     * @return void
     */
    public function delete($id = null) 
    {
        $httpSocket = new HttpSocket();
        $link = $this->get_url("rest_tarefas/$id.json");
        $response = $httpSocket->delete($link);
        $result = json_decode($response->body);

        if ($result->result) {
            $this->Session->setFlash(__('A tarefa foi removida.'));
        } else {
            $this->Session->setFlash(__('A tarefa não pôde ser removida. Por favor, tente novamente'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * update_priority method
     *
     * @param string $id
     * @return void
     */
    public function update_priority() 
    {
        $this->autoRender=false;
        
        if($this->RequestHandler->isAjax()){
            if(!empty($this->request->data)){
                $json = json_decode($this->request->data);
                $data = array();

                foreach ($json as $key => $value) {
                    array_push($data, array('Tarefa' => array('id' => $value->id, 'prioridade' => $key))); 
                }
                //Salvar multiplas linhas da tabela  
                $this->Tarefa->saveMany($data, array('deep' => true));
            }
        }
    }

    private function get_url($url) 
    {
        return Router::url("/$url", true);
    }
}
