$(document).ready(function() {
	
	var updateOutput = function(e)
    {
        var list   = e.length ? e : $(e.target);
        var formUrl = list.data("url");

        if (window.JSON) {
            var formData = window.JSON.stringify(list.nestable('serialize'))

            $.ajax({
                type: 'POST',
                url: formUrl,
                data: {"data": formData},
                error: function(xhr,textStatus,error){
                    alert(textStatus);
                }
            });	
        } 
    };


	$('.dd').nestable({
		maxDepth: 1
	})
	.on('change', updateOutput);
});