<div class="tarefas">
    <h2><?php echo __('Tarefas'); ?></h2>
    <div class="actions">
        <?php echo $this->Html->link(__('Nova Tarefa'), array('action' => 'add'), array('class' => 'btn btn-primary')); ?>
    </div>
    <div class="tarefas-list">
        <div class="row">
            <div class="col-sm-12">
                <div class="dd" id="nestable3" data-url="<?php echo Router::url(array('action' => 'update_priority')); ?>">
                    <ol class="dd-list">
                        <?php foreach ($tarefas->tarefas as $tarefa): ?>
                        <li class="dd-item dd3-item" data-id="<?php echo $tarefa->Tarefa->id; ?>">
                            <div class="dd-handle dd3-handle">Drag</div>
                            <div class="dd3-content">
                                <?php echo $this->Html->link(h($tarefa->Tarefa->titulo), array('action' => 'view', $tarefa->Tarefa->id)); ?>
                                
                                <div class="pull-right">
                                <?php echo $this->Html->link('<span class="glyphicon glyphicon-pencil"></span>' , array('action' => 'edit', $tarefa->Tarefa->id), array('escape' => false)); ?> &nbsp;
                                <?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span>', array('action' => 'delete', $tarefa->Tarefa->id), array('escape' => false, 'confirm' => __('Tem certeza de que deseja apagar "%s"?', $tarefa->Tarefa->titulo))); ?>
                                </div>
                            </div>
                        </li>
                        <?php endforeach; ?>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>