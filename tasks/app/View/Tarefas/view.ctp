<?php App::uses('CakeTime', 'Utility'); ?>
<?php $tarefa = $tarefa->tarefa; ?>

<div class="tarefas view">
    <h2><?php echo __('Tarefa'); ?></h2>
    <div class="actions">
        <li><?php echo $this->Html->link(__('List Tarefas'), array('action' => 'index')); ?> </li>
    </div>
    <dl>
        <dt><?php echo __('Titulo'); ?></dt>
        <dd>
            <?php echo h($tarefa->Tarefa->titulo); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Descricao'); ?></dt>
        <dd>
            <?php echo h($tarefa->Tarefa->descricao); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Criado em'); ?></dt>
        <dd>
            <?php echo CakeTime::format($tarefa->Tarefa->created, '%B %e, %Y %H:%M %p');?>
            &nbsp;
        </dd>
    </dl>
</div>