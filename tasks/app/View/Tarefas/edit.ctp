<div class="tarefas edit">
    <h2><?php echo __('Editar Tarefa'); ?></h2>
    <?php echo $this->Form->create('Tarefa'); ?>
        <?php
            echo $this->Form->input('id');
            echo $this->Form->input('titulo', array('div' => 'form-group', 'class' => 'form-control', 'placeholder' => 'Títuto da tarefa'));
            echo $this->Form->input('descricao', array('div' => 'form-group', 'class' => 'form-control', 'placeholder' => 'Descrição da tarefa'));
        ?>
        </fieldset>
    <?php echo $this->Form->end(array('label' => __('Salvar'), 'class' => 'btn btn-success', 'div' => 'form-group')); ?>
    </div>