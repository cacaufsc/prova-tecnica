<div class="tarefas new">
    <h2><?php echo __('Nova Tarefa'); ?></h2>
    <div class="actions">
        <?php echo $this->Html->link(__('Listar Tarefas'), array('action' => 'index'), array('class' => 'btn btn-link')); ?>
    </div>

 <?php echo $this->Form->create('Tarefa'); ?>
    <?php
        echo $this->Form->input('titulo', array('div' => 'form-group', 'class' => 'form-control', 'placeholder' => 'Títuto da tarefa'));
        echo $this->Form->input('descricao', array('div' => 'form-group', 'class' => 'form-control', 'placeholder' => 'Descrição da tarefa'));
    ?>
    <?php echo $this->Form->end(array('label' => __('Salvar'), 'class' => 'btn btn-success', 'div' => 'form-group')); ?>
</div>
